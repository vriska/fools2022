FROM devkitpro/devkitarm:20220216

RUN apt-get update && apt-get install -y build-essential binutils-arm-none-eabi git libpng-dev libncurses5 python2-dev
