@ 0x020182AD
EventScript_Password:
    lock
    faceplayer
    msgbox 0x02018993 @ "Wello! [...]"
    call EventScript_ClearStringVar2
    callnative 0x0203D075
    waitstate
    call EventScript_CheckPassword
    compare_var_to_value 0x800D, 0x0000
    goto_if TRUE, EventScript_WrongPassword
    msgbox 0x020189B6 @ "That's the correct [...]"
    callnative 0x02018233
    call 0x0203E17C
    playse 0x28
    callnative 0x02018263
    callnative DrawWholeMapView
    release
    end

@ 0x020182F3
EventScript_WrongPassword:
    @ ...

@ 0x0201835E
EventScript_CheckPassword:
    setvar 0x8001, 0x1B39
    loadbytefromptr 0, gStringVar2
    setptrbyte 0, 0x02018372
    addvar 0x8001, 0x0000 @ modified by above
    loadbytefromptr 0, gSpecialVar_0x8001
    setptrbyte 0, 0x02018399
    loadbytefromptr 0, gSpecialVar_0x8001 + 1
    setptrbyte 0, 0x0201839A
    setvar 0x8001, 0x0000
    setvar 0x8002, 0x49
    addvar 0x8001, 0x0000 @ modified by above
    subvar 0x8002, 0x01
    compare_var_to_value 0x8002, 0x0000
    goto_if 5, 0x02018396 @ !=
    addvar 0x8001, 0x18DF
    loadbytefromptr 0, gStringVar2 + 1
    setptrbyte 0, 0x020183BF
    addvar 0x8001, 0x0000 @ modified by above
    loadbytefromptr 0, gSpecialVar_0x8001
    setptrbyte 0, 0x020183E6
    loadbytefromptr 0, gSpecialVar_0x8001 

@ 0x020182FD
EventScript_ClearStringVar2:
    setptr 0xFF, gStringVar2
    setptr 0xFF, gStringVar2 + 1
    setptr 0xFF, gStringVar2 + 2
    @ ...
    setptr 0xFF, gStringVar2 + 15
    return
