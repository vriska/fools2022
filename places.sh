#!/usr/bin/env bash
loc_idx="$3"

while [[ "$loc_idx" -le 65000 ]]; do
    loc_be="$(printf '%04x\n' "$loc_idx")"
    loc="${loc_be:2:2} ${loc_be:0:2}"
    echo $loc
    loc_b64="$(echo "000000 01 A2 0A 41 $loc 11 1B" | xxd -r | base64)"
    curl -s --data "$loc_b64" -H 'User-Agent: vriska-places/1.0.0' -H "X-FoolsSessionToken: $2" "https://fools2022.online/turbopacket/$1" | base64 -d > maps/$loc_idx.bin
    wc -c maps/$loc_idx.bin
    sleep 0.5
    loc_idx="$((loc_idx + 1))"
done
