@ 0x020182B5
EventScript_Certificate:
    lock
    faceplayer
    loadbytefromptr 0, 0x020186F4
    compare_local_to_value 0, 0
    goto_if 1, EventScript_NoCertificate
    msgbox 0x02018442, MSGBOX_YESNO @ "Wello! I see you have a certificate [...]"
    compare_var_to_value 0x800D, 0x0000
    goto_if 1, EventScript_AppraisalDeclined
    callnative 0x02018253
    call 0x0203E17C
    callnative CheckCertificate
    msgbox 0x020184C7 @ "Here's your certificate!"
    loadbytefromptr 0, 0x02021DC0
    compare_local_to_value 0, 0x47
    goto_if 1, 0x02018348
EventScript_CertificateEnd:
    release
    end

@ 0x02018301
EventScript_NoCertificate:
    msgbox 0x0201835F, MSGBOX_YESNO @ "Wello! Do you want me to issue [...]"
    compare_var_to_value 0x800D, 0x0000
    goto_if 1, EventScript_CertificateEnd
EventScript_GenerateCertificate:
    msgbox 0x02018398 @ "Great! I'm going to generate [...]"
    callnative 0x02018235
    call 0x0203E17C
    msgbox 0x020183FB @ "Your certificate was received."
    release
    end

@ 0x02018330
EventScript_AppraisalDeclined:
    msgbox 0x02018489, MSGBOX_YESNO, @ "OK. In that case, should I generate a new [...]"
    compare_var_to_value 0x800D, 0x0000
    goto_if 1, EventScript_CertificateEnd
    goto EventScript_GenerateCertificate

@ 0x02018348
EventScript_GoldCertificate:
    msgbox 0x020184E3 @ "Wow, that's a Gold Certificate! [...]"
    playse 0x0028
    callnative 0x02018223
    callnative DrawWholeMapView
    release
    end

CheckCertificate:
   0x203d0af:   ldr     r3, [pc, #216]  ; (0x203d188)
   0x203d0b1:   ldrb    r0, [r3, #0]
   0x203d0b3:   cmp     r0, #255        ; 0xff
   0x203d0b5:   beq.n   0x203d0d2
   0x203d0b7:   movs    r1, #0
   0x203d0b9:   ldr     r2, [pc, #232]  ; (0x203d1a4)
   0x203d0bb:   adds    r2, r2, r1
   0x203d0bd:   ldrb    r2, [r2, #0]
   0x203d0bf:   cmp     r0, r2
   0x203d0c1:   beq.n   0x203d0c6
   0x203d0c3:   adds    r1, #1
   0x203d0c5:   b.n     0x203d0b8
   0x203d0c7:   ldr     r2, [pc, #224]  ; (0x203d1a8)
   0x203d0c9:   adds    r2, r2, r1
   0x203d0cb:   ldrb    r2, [r2, #0]
   0x203d0cd:   strb    r2, [r3, #0]
   0x203d0cf:   adds    r3, #1
   0x203d0d1:   b.n     0x203d0b0
   0x203d0d3:   bx      lr


