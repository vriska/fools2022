use anyhow::Result;
use std::io::prelude::*;
use std::net::{TcpListener, TcpStream};
use std::thread;

fn decode_char(byte: u8) -> char {
    match byte {
        0x00 => ' ',
        0x01..=0x03 => char::from_u32('À' as u32 + byte as u32 - 0x01).unwrap(),
        0x04 => 'Ç',
        0x05..=0x0C if byte != 0x0A => char::from_u32('È' as u32 + byte as u32 - 0x05).unwrap(),
        0x0D..=0x0F => char::from_u32('Ò' as u32 + byte as u32 - 0x0D).unwrap(),
        0x10 => 'Œ',
        0x11..=0x13 => char::from_u32('Ù' as u32 + byte as u32 - 0x11).unwrap(),
        0x14 => 'Ñ',
        0x15 => 'ß',
        0x16 => 'à',
        0x17 => 'á',
        0x19..=0x21 if byte != 0x1F => char::from_u32('ç' as u32 + byte as u32 - 0x19).unwrap(),
        0x22..=0x24 => char::from_u32('ò' as u32 + byte as u32 - 0x22).unwrap(),
        0x25 => 'œ',
        0x26..=0x28 => char::from_u32('ù' as u32 + byte as u32 - 0x26).unwrap(),
        0x29 => 'ñ',
        0x2A => 'º',
        0x2B => 'ª',
        0x2D => '&',
        0x2E => '+',
        0x35 => '=',
        0x36 => ';',
        0x50 => '▯',
        0x51 => '¿',
        0x52 => '¡',
        0x5A => 'Í',
        0x5B => '%',
        0x5C => '(',
        0x5D => ')',
        0x68 => 'â',
        0x6F => 'í',
        0x79 => '⬆',
        0x7A => '⬇',
        0x7B => '⬅',
        0x7C => '➡',
        0x84 => 'ᵉ',
        0x85 => '<',
        0x86 => '>',
        0xA1..=0xAA => char::from_u32('0' as u32 + byte as u32 - 0xA1).unwrap(),
        0xAB => '!',
        0xAC => '?',
        0xAD => '.',
        0xAE => '-',
        0xAF => '・',
        0xB0 => '…',
        0xB1 => '“',
        0xB2 => '”',
        0xB3 => '‘',
        0xB4 => '’',
        0xB5 => '♂',
        0xB6 => '♀',
        0xB7 => '¥', // pokédollar
        0xB8 => ',',
        0xB9 => '×',
        0xBA => '/',
        0xBB..=0xD4 => char::from_u32('A' as u32 + byte as u32 - 0xBB).unwrap(),
        0xD5..=0xEE => char::from_u32('a' as u32 + byte as u32 - 0xD5).unwrap(),
        0xEF => '▶',
        0xF0 => ':',
        0xF1 => 'Ä',
        0xF2 => 'Ö',
        0xF3 => 'Ü',
        0xF4 => 'ä',
        0xF5 => 'ö',
        0xF6 => 'ü',

        0x34 | 0x53..=0x59 | 0xA0 => '⌧', // not in unicode
        _ => char::REPLACEMENT_CHARACTER,   // invalid
    }
}

fn handle_connections(mut downstream: TcpStream, mut clock_downstream: TcpStream) -> Result<()> {
    let mut upstream = TcpStream::connect("127.0.0.1:54970")?;
    let mut clock_upstream = TcpStream::connect("127.0.0.1:49420")?;

    let mut buffer = [0u8; 6];
    let mut clock_buffer = [0u8; 4];

    let mut state = 0;
    let mut was_empty = false;

    loop {
        let len = clock_upstream.read(&mut clock_buffer)?;
        clock_downstream.write_all(&clock_buffer[..len])?;

        upstream.read_exact(&mut buffer[..1])?;

        let last_empty = was_empty;
        was_empty = false;

        match buffer[0] {
            0x00 | 0xFF => {
                println!(
                    "{}",
                    if buffer[0] == 0x00 {
                        "JOY_POLL"
                    } else {
                        "JOY_RESET"
                    }
                );
                downstream.write_all(&buffer[..1])?;
                downstream.read_exact(&mut buffer[1..4])?;
                upstream.write_all(&buffer[1..3])?;
                println!("\t-> {:02X}", buffer[3]);
            }
            0x14 => {
                downstream.write_all(&buffer[..1])?;
                downstream.read_exact(&mut buffer[1..6])?;

                if buffer[1..5] == [0x00, 0x00, 0x00, 0x00] {
                    was_empty = true;
                }

                if !was_empty || !last_empty {
                    println!("JOY_TRANS");
                    println!(
                        "\t-> {:02X} {:02X} {:02X} {:02X} \"{}\" \"{}\" (joy: {:02X})",
                        buffer[1],
                        buffer[2],
                        buffer[3],
                        buffer[4],
                        buffer[1..5]
                            .iter()
                            .copied()
                            .map(decode_char)
                            .collect::<String>(),
                        buffer[1..5]
                            .iter()
                            .copied()
                            .map(|x| char::from_u32(x as u32).unwrap())
                            .map(|x| if x.is_ascii_control() || !x.is_ascii() {
                                char::REPLACEMENT_CHARACTER
                            } else {
                                x
                            })
                            .collect::<String>(),
                        buffer[5]
                    );
                }

                /*if state == 0 && buffer[1..5] == [0x00, 0x00, 0x00, 0x00] {
                    println!("\tstate 0 -> 1");
                    state = 1;
                } else if state == 1 && buffer[1..5] == [0x02, 0x00, 0x00, 0x00] {
                    println!("\tstate 1 -> 2");
                    state = 2;
                } else if state == 2 {
                    println!("\tstate 2 -> 3");
                    state = 3;
                } else if state == 3 && buffer[1..3] == [0x8c, 0x31] {
                    println!("\tstate 3 hit");
                    state = 0;
                    //buffer[1] = 0x37;
                    //buffer[2] = 0x13;
                } else if state != 0 {
                    println!("\tstate {} -> 0", state);
                    state = 0;
                } else {
                    state = 0;
                }*/

                if state == 0 && buffer[1..5] == [0xC0, 0xBB, 0xBC, 0xCF] {
                    println!("\tstate 0 -> 1");
                    state = 1;
                    buffer[1] = 0xBB + (b'V' - b'A');
                    buffer[2] = 0xBB + (b'R' - b'A');
                    buffer[3] = 0xBB + (b'I' - b'A');
                    buffer[4] = 0xBB + (b'S' - b'A');
                } else if state == 1 && buffer[1..5] == [0xC6, 0xC9, 0xCF, 0xCD] {
                    println!("\tstate 1 -> 2");
                    state = 2;
                    buffer[1] = 0xBB + (b'K' - b'A');
                    buffer[2] = 0xBB;
                    buffer[3] = 0x00;
                    buffer[4] = 0xBB + (b'S' - b'A');
                } else if state == 2 && buffer[1..5] == [0x00, 0xD1, 0xC9, 0xCC] {
                    println!("\tstate 2 -> 3");
                    state = 3;
                    buffer[1] = 0xBB + (b'E' - b'A');
                    buffer[2] = 0xBB + (b'R' - b'A');
                    buffer[3] = 0xBB + (b'K' - b'A');
                    buffer[4] = 0xBB + (b'E' - b'A');
                } else if state == 3 && buffer[1..3] == [0xBE, 0xFF] {
                    println!("\tstate 3 hit");
                    state = 0;
                    buffer[1] = 0xBB + (b'T' - b'A');
                } else if state != 0 {
                    println!("\tstate {} -> 0", state);
                    state = 0;
                } else {
                    state = 0;
                }

                upstream.write_all(&buffer[1..6])?;
            }
            0x15 => {
                println!("JOY_RECV");
                upstream.read_exact(&mut buffer[1..5])?;
                downstream.write_all(&buffer[..5])?;
                println!(
                    "\t<- {:02X} {:02X} {:02X} {:02X} \"{}\" \"{}\"",
                    buffer[1],
                    buffer[2],
                    buffer[3],
                    buffer[4],
                    buffer[1..5]
                        .iter()
                        .copied()
                        .map(decode_char)
                        .collect::<String>(),
                    buffer[1..5]
                        .iter()
                        .copied()
                        .map(|x| char::from_u32(x as u32).unwrap())
                        .map(|x| if x.is_ascii_control() || !x.is_ascii() {
                            char::REPLACEMENT_CHARACTER
                        } else {
                            x
                        })
                        .collect::<String>(),
                );
                downstream.read_exact(&mut buffer[1..2])?;
                upstream.write_all(&buffer[1..2])?;
                println!("\t-> (joy: {:02X})", buffer[1]);
            }
            other => anyhow::bail!("unknown command 0x{:02X}", other),
        }
    }
}

fn main() -> Result<()> {
    let data_listener = TcpListener::bind("127.0.0.2:54970")?;
    let clock_listener = TcpListener::bind("127.0.0.2:49420")?;

    for data_downstream in data_listener.incoming() {
        let data_downstream = data_downstream?;

        let clock_downstream = clock_listener.incoming().next().unwrap()?;
        thread::spawn(|| handle_connections(data_downstream, clock_downstream).unwrap());
    }
    Ok(())
}
