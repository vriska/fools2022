#!/usr/bin/env python3
from z3 import BitVec, Solver, Or, And

def compute_result(string, constants, last):
    result = 0x0000

    for byte, (value, iterations) in zip(string, constants):
        result = result + value
        result = byte + result
        result = result * iterations

    return result + last

constants_a = [
    (0x1B39, 0x49),
    (0x18DF, 0x61),
    (0x13EB, 0x0D),
    (0x11EF, 0x29),
    (0x1145, 0x43),
    (0x12DF, 0x65),
    (0x0DFD, 0x59),
    (0x13AF, 0x8B),
    (0x149F, 0x47),
    (0x0FEF, 0x53),
]
last_a = 0x0FB5
result_a = 0xB0EF
constants_b = [
    (0x0539, 0x3B),
    (0x0E75, 0xB5),
    (0x11FB, 0x7F),
    (0x1237, 0xA3),
    (0x125F, 0x67),
    (0x107B, 0xA3),
    (0x1951, 0x95),
    (0x1B47, 0xC1),
    (0x151F, 0xD3),
    (0x14B1, 0x97),
]
last_b = 0x13EB
result_b = 0xD4B9

s = Solver()

string = [BitVec(f'str_{x}', 16) for x in range(10)]

for byte in string:
    s.add(Or(
        byte == 0xAD, # .
        byte == 0xB8, # ,
        And(byte >= 0xBB, byte <= 0xD4), # upper
        And(byte >= 0xD5, byte <= 0xEE), # lower
        And(byte >= 0xA1, byte <= 0xAA), # numbers
        byte == 0xAB, # !
        byte == 0xAC, # ?
        byte == 0xB5, # male
        byte == 0xB6, # female
        byte == 0xBA, # /
        byte == 0xAE, # -
        byte == 0xB0, # ...
        byte == 0xB1, # "
        byte == 0xB2, # "
        byte == 0xB3, # '
        byte == 0xB4, # '
    ))

s.add(compute_result(string, constants_a, last_a) == result_a)
s.add(compute_result(string, constants_b, last_b) == result_b)

print(s.check())

m = s.model()
print(', '.join((hex(m[byte].as_long()) for byte in string)))
