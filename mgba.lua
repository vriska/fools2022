mgba_protocol = Proto("mGBA", "mGBA Protocol")

command = ProtoField.uint8("mgba.command", "command", base.HEX)
payload = ProtoField.bytes("mgba.payload", "payload")
joy = ProtoField.uint8("mgba.joy", "joy", base.HEX)

mgba_protocol.fields = { command, payload, joy }

function get_command_name(command)
    local command_name = "Unknown"

    if command == 0xFF then
        command_name = "JOY_RESET"
    elseif command == 0x00 then
        command_name = "JOY_POLL"
    elseif command == 0x14 then
        command_name = "JOY_TRANS"
    elseif command == 0x15 then
        command_name = "JOY_RECV"
    end

    return command_name
end

function mgba_protocol.dissector(buffer, pinfo, tree)
    length = buffer:len()
    if length == 0 then return end

    pinfo.cols.protocol = mgba_protocol.name

    local subtree = tree:add(mgba_protocol, buffer(), "mGBA Protocol")

    if pinfo.src_port == 54970 then
        local command_number = buffer(0, 1):le_uint()
        local command_name = get_command_name(command_number)
        subtree:add_le(command, buffer(0, 1)):append_text(" (" .. command_name .. ")")

        if command_number == 0x15 then
            subtree:add(payload, buffer(1))
        end
    elseif length == 3 then
        subtree:add_le(joy, buffer(2, 1))
    elseif length == 1 then
        subtree:add_le(joy, buffer(0, 1))
    elseif length == 5 then
        subtree:add(payload, buffer(0, 4))
        subtree:add_le(joy, buffer(4, 1))
    end
end

local tcp_port = DissectorTable.get("tcp.port")
tcp_port:add(54970, mgba_protocol)
