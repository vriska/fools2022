#!/usr/bin/env bash
set -e
cd "$(dirname "$0")/.."
export WINEPREFIX="$PWD/.wine"
cd data/Fools2022Client/
wine main.exe
