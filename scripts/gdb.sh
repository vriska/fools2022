#!/usr/bin/env bash
set -e
cd "$(dirname "$0")/.."
docker run --rm -it -v "$PWD/data:$PWD/data" --net=host pokeemerald bash -c "/opt/devkitpro/devkitARM/bin/arm-none-eabi-gdb -ex 'target extended-remote 127.0.0.1:2345' -ex continue $PWD/data/pokeemerald/pokeemerald.elf"
