#!/usr/bin/env bash
set -e
cd "$(dirname "$0")/.."
tcpdump -i any -nn -w "$1" --immediate-mode 'host 154.53.55.38 or port 54970'
