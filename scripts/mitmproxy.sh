#!/usr/bin/env bash
set -e
cd "$(dirname "$0")/.."
SSLKEYLOGFILE="$PWD/.mitmproxy/sslkeylogfile.txt" mitmproxy --set confdir="$PWD/.mitmproxy" --mode transparent --showhost
