#!/usr/bin/env bash
set -e
cd "$(dirname "$0")/.."
cd data
cp -v fools2022_v1.01.sav pokeemerald/pokeemerald.sav
mgba-qt ./pokeemerald/pokeemerald.gba
