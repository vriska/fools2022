#!/usr/bin/env bash
set -e
cd "$(dirname "$0")/.."
docker run --rm -it -v "$PWD/data:$PWD/data" pokeemerald bash -c "cd $PWD/data/agbcc && ./build.sh && ./install.sh ../pokeemerald"
