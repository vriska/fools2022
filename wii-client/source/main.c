#include <stdio.h>
#include <stdlib.h>
#include <gccore.h>
#include <network.h>
#include <unistd.h>
#include <malloc.h>
#include <wiiuse/wpad.h>
#include <ogc/lwp_watchdog.h>
#include <string.h>
#include "../bearssl/bearssl.h"
#include "creds.h"
#include "cJSON.h"
#include "b64.h"

static void *xfb = NULL;
static GXRModeObj *rmode = NULL;

static int sock_read(void* ctx, unsigned char* buf, size_t len) {
    ssize_t rlen;

    rlen = net_read(*(int*)ctx, buf, len);
    if (rlen <= 0) return -1;
    return (int) rlen;
}

static int sock_write(void* ctx, const unsigned char* buf, size_t len) {
    ssize_t wlen;

    wlen = net_write(*(int*)ctx, buf, len);
    if (wlen <= 0) return -1;
    return (int) wlen;
}

static const unsigned char TA0_DN[] = {
        0x30, 0x1B, 0x31, 0x19, 0x30, 0x17, 0x06, 0x03, 0x55, 0x04, 0x03, 0x13,
        0x10, 0x66, 0x6F, 0x6F, 0x6C, 0x73, 0x32, 0x30, 0x32, 0x32, 0x2E, 0x6F,
        0x6E, 0x6C, 0x69, 0x6E, 0x65
};

static const unsigned char TA0_RSA_N[] = {
        0xA9, 0xEB, 0x80, 0xB8, 0xFA, 0x9F, 0xCF, 0xFA, 0x17, 0x16, 0xA9, 0x3A,
        0xC1, 0x3F, 0xB5, 0x1C, 0x75, 0x8D, 0x84, 0xDA, 0xD2, 0xC5, 0xFF, 0x4B,
        0xA9, 0x9C, 0x18, 0xDC, 0x34, 0xC6, 0xB3, 0xFD, 0xF8, 0xEE, 0x38, 0xC6,
        0x69, 0x7A, 0x42, 0xB3, 0x63, 0x19, 0x74, 0xFD, 0x32, 0xA3, 0xFA, 0xFD,
        0xE5, 0xC7, 0xF5, 0x3E, 0x8D, 0x3D, 0x07, 0x49, 0x6B, 0xB9, 0x56, 0x1E,
        0x7F, 0xBE, 0x56, 0x5C, 0xBC, 0xCC, 0x78, 0x46, 0x23, 0x26, 0xE9, 0xF3,
        0x39, 0x4F, 0x36, 0xA2, 0xE2, 0xB1, 0x89, 0x26, 0xAB, 0x9F, 0x9C, 0xFC,
        0xF3, 0x55, 0xE5, 0x9C, 0xD4, 0xE6, 0x62, 0xA2, 0x98, 0x63, 0x76, 0x9A,
        0x37, 0xFD, 0xFE, 0xBC, 0xB8, 0x3D, 0x6D, 0xA9, 0xB5, 0xC1, 0x57, 0xC8,
        0x67, 0xF6, 0xA0, 0x0F, 0x2F, 0xA9, 0xA5, 0x92, 0xC6, 0x87, 0xB5, 0x29,
        0x93, 0x7B, 0x41, 0x59, 0x67, 0xF1, 0x3F, 0x9F, 0x9A, 0x76, 0xAA, 0x79,
        0xCF, 0x15, 0x8A, 0xCA, 0xBD, 0x13, 0xF4, 0x3B, 0xFB, 0x41, 0x02, 0xA7,
        0x91, 0x1D, 0xB2, 0x60, 0xEA, 0x4D, 0x25, 0x6B, 0x63, 0x52, 0xA7, 0xF2,
        0x8B, 0xCE, 0x49, 0x02, 0xFA, 0xF9, 0xE9, 0xA0, 0x5E, 0x86, 0x32, 0x24,
        0x99, 0xBE, 0xD7, 0x7A, 0x49, 0xEF, 0x46, 0x88, 0xE3, 0xCB, 0x39, 0x87,
        0x1A, 0x73, 0xF6, 0xBF, 0xC1, 0x32, 0xFF, 0xA7, 0xEF, 0x6C, 0x76, 0x77,
        0x94, 0x95, 0x24, 0xF6, 0xD2, 0x49, 0x59, 0xB5, 0xEB, 0xD5, 0xF4, 0xEE,
        0x32, 0x80, 0xF6, 0x1A, 0xD7, 0x84, 0xE2, 0x5F, 0x7C, 0xDE, 0x3B, 0xB2,
        0xBE, 0xAF, 0x1D, 0xF6, 0xAD, 0xAA, 0x99, 0x6C, 0x49, 0xE8, 0xFF, 0x1C,
        0x38, 0xAC, 0xFF, 0x6B, 0xC3, 0x9F, 0xAA, 0xFF, 0x5B, 0x1B, 0x8E, 0x37,
        0x31, 0xF9, 0x37, 0x86, 0xE7, 0x3D, 0x44, 0x8D, 0x1F, 0x64, 0x02, 0x94,
        0xAC, 0x9F, 0x83, 0x47
};

static const unsigned char TA0_RSA_E[] = {
        0x01, 0x00, 0x01
};

static const br_x509_trust_anchor TAs[1] = {
        {
                { (unsigned char *)TA0_DN, sizeof TA0_DN },
                0,
                {
                        BR_KEYTYPE_RSA,
                        { .rsa = {
                                (unsigned char *)TA0_RSA_N, sizeof TA0_RSA_N,
                                (unsigned char *)TA0_RSA_E, sizeof TA0_RSA_E,
                        } }
                }
        }
};

#define TAs_NUM   1

char session_token[256];
int uid;

int send_packet(u8* packet, u32* packet_len, int buf_len) {
    usleep(500 * 1000);

    int sock = net_socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
    if (sock == INVALID_SOCKET) {
        printf("Failed to create socket!\n");
        sleep(5);
        return 1;
    }

    struct sockaddr_in server = {0};
    server.sin_family = AF_INET;
    server.sin_port = htons(443);
    if (inet_aton("154.53.55.38", &server.sin_addr) == 0) {
        printf("Invalid address!\n");
        return 1;
    }

    if (net_connect(sock, (struct sockaddr*) &server, sizeof(server)) < 0) {
        printf("Error connecting!\n");
        return 1;
    }

    br_ssl_client_context sc;
    br_x509_minimal_context xc;
    br_ssl_client_init_full(&sc, &xc, TAs, TAs_NUM);

    unsigned char iobuf[BR_SSL_BUFSIZE_BIDI];
    br_ssl_engine_set_buffer(&sc.eng, iobuf, sizeof(iobuf), 1);

    // security™
    srand(gettick());
    for (int i = 0; i < 8; i++) {
        int num = rand();
        br_ssl_engine_inject_entropy(&sc.eng, &num, sizeof(num));
    }

    if (!br_ssl_client_reset(&sc, "fools2022.online", 0)) {
        printf("Failed to reset client!\n");
        net_close(sock);
        return 1;
    }

    br_sslio_context ioc;
    br_sslio_init(&ioc, &sc.eng, sock_read, &sock, sock_write, &sock);

    char* base64_input = b64_encode(packet, *packet_len * 4);
    printf("%s\n", base64_input);

    const int buffer_size = 1024 * 1024;
    char* buffer = calloc(buffer_size, 1);
    int buffer_len = snprintf(buffer, buffer_size,
            "POST /turbopacket/%d HTTP/1.1\r\n"
            "Host: fools2022.online\r\n"
            "User-Agent: vriska-wii/1.0.0\r\n"
            "Connection: close\r\n"
            "Content-Length: %lu\r\n"
            "X-FoolsSessionToken: %s\r\n\r\n%s",
            uid, (unsigned long) strlen(base64_input), session_token, base64_input);
    free(base64_input);

    if (buffer_len >= buffer_size) {
        printf("Request truncated! Full buffer %d bytes\n", buffer_len);
        free(buffer);
        net_close(sock);
        return 1;
    }

    if (br_sslio_write_all(&ioc, buffer, buffer_len)) {
        printf("Failed to write request! %d\n", br_ssl_engine_last_error(&sc.eng));
        free(buffer);
        net_close(sock);
        return 1;
    }
    if (br_sslio_flush(&ioc)) {
        printf("Failed to flush request!\n");
        free(buffer);
        net_close(sock);
        return 1;
    }

    buffer_len = 0;

    while (1) {
        int rlen = br_sslio_read(&ioc, buffer, buffer_size - buffer_len);
        buffer_len += rlen;

        if (rlen < 0) {
            break;
        } else if (rlen == 0 || buffer_len >= buffer_size) {
            printf("Response too long!\n");
            free(buffer);
            net_close(sock);
            return 1;
        }
    }

    char* refresh = strstr(buffer, "X-FoolsRefreshToken: ");
    if (refresh) {
        refresh += strlen("X-FoolsRefreshToken: ");
        char* refresh_end = strchr(refresh, '\r');
        if (refresh_end) {
            int len = refresh_end - refresh;
            memcpy(session_token, refresh, len);
            session_token[len] = 0;
        }
    }

    char* data = strstr(buffer, "\r\n\r\n");
    if (!data) {
        printf("Invalid response!\n");
        free(buffer);
        net_close(sock);
        return 1;
    }
    data += 4;
    char* chunk = strstr(data, "\r\n");
    if (chunk) {
        data = chunk + 2;
        char* data_end = strstr(data, "\r\n0");
        *data_end = 0;
    }

    printf("%s\n", data);

    size_t new_size = 0;
    unsigned char* decoded = b64_decode_ex(data, strlen(data), &new_size);
    if (new_size >= buf_len) {
        printf("Response too large! %lu bytes\n", (unsigned long) new_size);
        free(decoded);
        free(buffer);
        net_close(sock);
        return 1;
    }
    *packet_len = new_size / 4;
    memcpy(packet, decoded, new_size);

    free(buffer);
    net_close(sock);

    return 0;
}

int log_in() {
    int sock = net_socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
    if (sock == INVALID_SOCKET) {
        printf("Failed to create socket!\n");
        sleep(5);
        return 1;
    }

    struct sockaddr_in server = {0};
    server.sin_family = AF_INET;
    server.sin_port = htons(443);
    if (inet_aton("154.53.55.38", &server.sin_addr) == 0) {
        printf("Invalid address!\n");
        return 1;
    }

    if (net_connect(sock, (struct sockaddr*) &server, sizeof(server)) < 0) {
        printf("Error connecting!\n");
        return 1;
    }

    br_ssl_client_context sc;
    br_x509_minimal_context xc;
    br_ssl_client_init_full(&sc, &xc, TAs, TAs_NUM);

    unsigned char iobuf[BR_SSL_BUFSIZE_BIDI];
    br_ssl_engine_set_buffer(&sc.eng, iobuf, sizeof(iobuf), 1);

    // security™
    srand(time(NULL));
    for (int i = 0; i < 8; i++) {
        int num = rand();
        br_ssl_engine_inject_entropy(&sc.eng, &num, sizeof(num));
    }

    if (!br_ssl_client_reset(&sc, "fools2022.online", 0)) {
        printf("Failed to reset client!\n");
        net_close(sock);
        return 1;
    }

    br_sslio_context ioc;
    br_sslio_init(&ioc, &sc.eng, sock_read, &sock, sock_write, &sock);

    const int buffer_size = 1024 * 1024;
    char* buffer = calloc(buffer_size, 1);
    int buffer_len = snprintf(buffer, buffer_size,
            "POST /login HTTP/1.1\r\n"
            "Host: fools2022.online\r\n"
            "User-Agent: vriska-wii/1.0.0\r\n"
            "Connection: close\r\n"
            "Content-Length: %lu\r\n"
            "\r\n"
            "{\"u\":\"%s\",\"p\":\"%s\"}",
            (unsigned long) (strlen(ACCOUNT_USERNAME) + strlen(ACCOUNT_PASSWORD) + strlen("{\"u\":\"\",\"p\":\"\"}")),
            ACCOUNT_USERNAME, ACCOUNT_PASSWORD);

    if (buffer_len >= buffer_size) {
        printf("Request truncated! Full buffer %d bytes\n", buffer_len);
        free(buffer);
        net_close(sock);
        return 1;
    }

    if (br_sslio_write_all(&ioc, buffer, buffer_len)) {
        printf("Failed to write request! %d\n", br_ssl_engine_last_error(&sc.eng));
        free(buffer);
        net_close(sock);
        return 1;
    }
    if (br_sslio_flush(&ioc)) {
        printf("Failed to flush request!\n");
        free(buffer);
        net_close(sock);
        return 1;
    }

    buffer_len = 0;

    while (1) {
        int rlen = br_sslio_read(&ioc, buffer, buffer_size - buffer_len);
        buffer_len += rlen;

        if (rlen < 0) {
            break;
        } else if (rlen == 0 || buffer_len >= buffer_size) {
            printf("Response too long!\n");
            free(buffer);
            net_close(sock);
            return 1;
        }
    }

    char* data = strstr(buffer, "\r\n\r\n");
    if (!data) {
        printf("Invalid response!\n");
        free(buffer);
        net_close(sock);
        return 1;
    }
    data += 4;
    char* chunk = strstr(data, "\r\n");
    if (chunk) {
        data = chunk + 2;
    }

    cJSON* json = cJSON_Parse(data);
    const cJSON* json_data = cJSON_GetObjectItemCaseSensitive(json, "data");
    if (!json_data) {
        printf("Missing data!\n");
        cJSON_Delete(json);
        free(buffer);
        net_close(sock);
        return 1;
    }

    const cJSON* json_session = cJSON_GetObjectItemCaseSensitive(json_data, "session");
    const cJSON* json_uid = cJSON_GetObjectItemCaseSensitive(json_data, "uid");
    if (!json_session || !json_uid || !cJSON_IsString(json_session) || !json_session->valuestring || !cJSON_IsNumber(json_uid)) {
        printf("Invalid data!\n");
        cJSON_Delete(json);
        free(buffer);
        net_close(sock);
        return 1;
    }

    uid = (int) json_uid->valuedouble;
    strncpy(session_token, json_session->valuestring, sizeof(session_token) - 1);

    cJSON_Delete(json);
    free(buffer);
    net_close(sock);

    return 0;
}

volatile u32 transval = 0;
void transfer_cb(s32 chan, u32 ret) {
    transval = 1;
}

volatile u32 resval = 0;
void type_cb(s32 res, u32 val) {
    resval = val;
}

u8* resbuf;
u8* cmdbuf;

u32 recv() {
    memset(resbuf, 0, 32);
    cmdbuf[0] = 0x14; //read
    transval = 0;
    SI_Transfer(1, cmdbuf, 1, resbuf, 5, transfer_cb, 50);
    while (transval == 0) ;
    u32 resp = *(vu32*)resbuf;
    return __builtin_bswap32(resp);
}

void send(u32 msg) {
    cmdbuf[0] = 0x15;
    cmdbuf[1] = (msg>>0) & 0xFF;
    cmdbuf[2] = (msg>>8) & 0xFF;
    cmdbuf[3] = (msg>>16) & 0xFF;
    cmdbuf[4] = (msg>>24) & 0xFF;
    transval = 0;
    resbuf[0] = 0;
    SI_Transfer(1, cmdbuf, 5, resbuf, 1, transfer_cb, 50);
    while(transval == 0) ;
}

typedef enum state_t {
    SHF = 1,
    SHS = 2,
    SR1 = 3,
    SR2 = 4,
    SRL = 5,
    SRD = 6,
    SPP = 7,
    SWL = 8,
    SWR = 9,
} state_t;

int main(int argc, char **argv) {
    // Initialise the video system
    VIDEO_Init();

    // This function initialises the attached controllers
    WPAD_Init();

    // Obtain the preferred video mode from the system
    // This will correspond to the settings in the Wii menu
    rmode = VIDEO_GetPreferredMode(NULL);

    // Allocate memory for the display in the uncached region
    xfb = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));

    // Initialise the console, required for printf
    console_init(xfb, 20, 20, rmode->fbWidth, rmode->xfbHeight, rmode->fbWidth*VI_DISPLAY_PIX_SZ);

    // Set up the video registers with the chosen mode
    VIDEO_Configure(rmode);

    // Tell the video hardware where our display memory is
    VIDEO_SetNextFramebuffer(xfb);

    // Make the display visible
    VIDEO_SetBlack(FALSE);

    // Flush the video register changes to the hardware
    VIDEO_Flush();

    // Wait for Video setup to complete
    VIDEO_WaitVSync();
    if (rmode->viTVMode&VI_NON_INTERLACE) VIDEO_WaitVSync();

    char localip[16] = {0};
    char gateway[16] = {0};
    char netmask[16] = {0};

    printf("Configuring network...\n");

    if (if_config(localip, netmask, gateway, true, 20) < 0) {
        printf("Configuration failed!\n");
        sleep(5);
        return 1;
    }

    printf("ip %s gw %s mask %s\n", localip, gateway, netmask);

    PAD_Init();

    if (log_in()) {
        while (true) {
            VIDEO_WaitVSync();
            PAD_ScanPads();
            if (PAD_ButtonsDown(0)) {
                return 0;
            }
        }
    }

    cmdbuf = memalign(32, 32);
    resbuf = memalign(32, 32);

    while (1) {
        printf("Waiting for GBA...\n");

        resval = 0;

        SI_GetTypeAsync(1, type_cb);
        while (1) {
            if (resval) {
                if (resval == 0x80 || resval & 8) {
                    resval = 0;
                    SI_GetTypeAsync(1, type_cb);
                } else {
                    break;
                }
            }
            VIDEO_WaitVSync();
        }

        if (resval & SI_GBA) {
            printf("GBA found!\n");
            state_t cur_state = SHF;
            u32 packet_len = 0;
            const int buf_len = 1024 * 1024;
            u8* packet = calloc(buf_len, 1);
            u32 idx = 0;
            u32 remaining = 0;
            while (1) {
                u32 data = recv();
                if (data || (cur_state != SHF && cur_state != SRL)) printf("%d %x\n", cur_state, data);

                if (data == 0xbe5015ff) {
                    idx = 0;
                    cur_state = SHS;
                } else if (cur_state == SHS && data == 0xffbe5015) {
                    send(0x000f0015);
                    send(0x20221337);
                    cur_state = SR1;
                } else if (cur_state == SR1) {
                    cur_state = SR2;
                } else if (cur_state == SR2) {
                    cur_state = SRL;
                } else if (cur_state == SRL && data != 0) {
                    packet_len = data;
                    remaining = data;
                    cur_state = SRD;
                } else if (cur_state == SRD) {
                    packet[idx++] = data >> 0  & 255;
                    packet[idx++] = data >> 8  & 255;
                    packet[idx++] = data >> 16 & 255;
                    packet[idx++] = data >> 24 & 255;
                    remaining--;
                    if (remaining == 0) {
                        if (send_packet(packet, &packet_len, buf_len)) {
                            while (true) {
                                VIDEO_WaitVSync();
                                PAD_ScanPads();
                                if (PAD_ButtonsDown(0)) {
                                    return 0;
                                }
                            }
                        }
                        cur_state = SWR;
                        printf("%d\n", packet_len);
                        send(0xfef0cec0);
                        send(0xcaceface);
                        send(packet_len + 0x65000000);
                    }
                } else if (cur_state == SWR) {
                    idx = 0;
                    for (int i = 0; i < packet_len * 4; i += 4) {
                        u32 data = 0;
                        data |= packet[i + 0] << 0;
                        data |= packet[i + 1] << 8;
                        data |= packet[i + 2] << 16;
                        data |= packet[i + 3] << 24;
                        send(data);
                    }
                    cur_state = SHF;
                }
                VIDEO_WaitVSync();
            }
        }
    }

    return 0;
}
