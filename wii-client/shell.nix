# derived from https://gist.github.com/TomSmeets/e306c414216def4fc8c12d0914717a7c
{ pkgs ? import <nixpkgs> { } }: with pkgs; rec {
  devkitppc-img = dockerTools.pullImage {
    imageName = "devkitpro/devkitppc";
    imageDigest = "sha256:77ed88cb417e057fa805e12a8ce1eab8865fe35a761cde7be00315d5c6cba288";
    sha256 = "LLFLDSPJ/tCRBLj0f9q34b5GVHnHudFCgkb7ppMm8VI=";
    finalImageName = "devkitpro/devkitppc";
    finalImageTag = "20200704";
  };

  # based on <nixpkgs/nixos/modules/virtualisation/docker-preloader.nix>
  extractDocker = image: dir: pkgs.vmTools.runInLinuxVM (
    pkgs.runCommand "docker-preload-image"
      {
        buildInputs = with pkgs; [ docker e2fsprogs utillinux curl kmod ];
        # 4 gb was not enough, 8 gb is aparently
        memSize = 1024 * 8;
      }
      ''
        modprobe overlay

        # from https://github.com/tianon/cgroupfs-mount/blob/master/cgroupfs-mount
        mount -t tmpfs -o uid=0,gid=0,mode=0755 cgroup /sys/fs/cgroup
        cd /sys/fs/cgroup
        for sys in $(awk '!/^#/ { if ($4 == 1) print $1 }' /proc/cgroups); do
          mkdir -p $sys
          if ! mountpoint -q $sys; then
            if ! mount -n -t cgroup -o $sys cgroup $sys; then
              rmdir $sys || true
            fi
          fi
        done

        dockerd -H tcp://127.0.0.1:5555 -H unix:///var/run/docker.sock &

        until $(curl --output /dev/null --silent --connect-timeout 2 http://127.0.0.1:5555); do
          printf '.'
          sleep 1
        done

        echo load image
        docker load -i ${image}

        echo run image
        docker run ${image.destNameTag} tar -C '${toString dir}' -c . | tar -xv --no-same-owner -C $out || true

        echo end
        kill %1
      ''
  );

  mllib = rec {
    mllib = pkgs.fetchzip {
      url = https://github.com/Minishlink/MLlib/releases/download/v1.3/MLlib_190111.zip;
      sha256 = "1r7lbbvissk6fscqzpacward6s8lil818lhlj81rq5y7mgnrhi3c";
    };
    others = pkgs.fetchzip {
      url = https://github.com/Minishlink/MLlib/releases/download/v1.3/MLlib_Others_190111.zip;
      sha256 = "1lms8xn8yhzjnprs3id4fhbgrbi5hp78sjcyc8qxhxawlv2xy87p";
    };
    png = pkgs.fetchzip {
      url = https://github.com/Minishlink/MLlib/releases/download/v1.3/libpng_wii.zip;
      sha256 = "08z7gqyhjr3widbfc0sy7vyb6j0zp8hwlnkkgvjnzyxgkbf1kqkk";
    };
    libz = pkgs.fetchzip {
      url = https://github.com/Minishlink/MLlib/releases/download/v1.3/libz_wii.zip;
      sha256 = "0dl4p56gc17cdwn6hlb6p3yxxgxjszj26mynl1cljdkzd19lnmr4";
      stripRoot = false;
    };
    freetype =
      pkgs.runCommand "FreeType-PPC"
        {
          src = pkgs.fetchurl {
            url = https://wiibrew.org/w/images/3/3d/FreeType-PPC.rar;
            sha256 = "1ss74aq1msv7jx11kp7p17s7al3ggfvfv0jfcf7v0hzvw56vp1rv";
          };
          buildInputs = [ unrar ];
        } ''
        mkdir $out
        cd $out
        unrar x $src
      '';

    combined =
      pkgs.runCommand "mllib"
        { } ''
        mkdir -p $out
        cd $out


        cp -r ${devkit}/*     $out

        chmod -R +rwx $out

        cp -r ${mllib}        ./MLlib
        cp -r ${others}       ./MLlib_Others

        cp -r ${png}/png      ./libogc/include/png
        cp -r ${png}/libpng.a ./libogc/lib/wii/libpng.a

        cp -r ${libz}/*.h     ./libogc/include/
        cp -r ${libz}/libz.a  ./libogc/lib/wii/libz.a

        cp -r ${freetype}/lib/cube/libfreetype.a ./libogc/lib/cube/libfreetype.a
        cp -r ${freetype}/lib/wii/libfreetype.a  ./libogc/lib/wii/libfreetype.a
        cp -r ${freetype}/include/*              ./libogc/include/
      '';

    env = pkgs.mkShell rec {
      DEVKITPRO = "${combined}";
      DEVKITPPC = "${combined}/devkitPPC";
      MLPATH = "${combined}/MLlib";
      shellHook = ''
      export PATH="$DEVKITPRO/devkitPPC/bin:$DEVKITPRO/tools/bin:$PATH"
      '';
    };
  };

  devkit = stdenv.mkDerivation {
    name = "devkitpro";
    src = extractDocker devkitppc-img "/opt/devkitpro";
    nativeBuildInputs = [ autoPatchelfHook ];
    buildInputs = [
      stdenv.cc.cc
      openssl
      zlib
      libarchive
      ncurses5
      expat
      tlf
    ];

    buildPhase = "true";
    installPhase = ''
      mkdir -p $out
      cp -r $src/{devkitPPC,libogc,examples,portlibs,tools,wut} $out
      rm -rf $out/pacman
    '';
  };
}.mllib.env
